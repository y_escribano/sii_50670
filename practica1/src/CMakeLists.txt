INCLUDE_DIRECTORIES("${PROJECT_INCLUDE_DIR}")
FIND_PACKAGE(Threads)

SET(COMMON_SRC 
	MundoCliente.cpp
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp)
SET(COMMON_SRS 
	MundoServidor.cpp 
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp)
				

ADD_EXECUTABLE(logger logger.cpp)
ADD_EXECUTABLE(bot bot.cpp)
ADD_EXECUTABLE(cliente cliente.cpp ${COMMON_SRC})
ADD_EXECUTABLE(servidor servidor.cpp ${COMMON_SRS})


TARGET_LINK_LIBRARIES(servidor glut GL GLU)
TARGET_LINK_LIBRARIES(cliente glut GL GLU)

target_link_libraries (servidor ${CMAKE_THREAD_LIBS_INIT})
