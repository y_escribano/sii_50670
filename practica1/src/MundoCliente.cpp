// Mundocliente.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


  #include <sys/types.h>
  #include <sys/stat.h>
  #include <fcntl.h> 
  #include <unistd.h>
  #include <errno.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	//close(tuberia);
	close(fifosc);
	unlink("/tmp/Tuberiasc");
	close(fifocs);
	unlink("/tmp/Tuberiacs");
	
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);


	m->raqueta1=jugador1;
	m->esfera=esfera;

	if(m->accion==1)OnKeyboardDown('w',0,0);
	else if(m->accion==-1)OnKeyboardDown('s',0,0);


	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		char cad[100];
		sprintf(cad," jugador 2 marco 1 punto,lleva %d \n",puntos2);
		write (tuberia,cad,strlen(cad)+1);

		if (esfera.radio>0.1f)
			{
				esfera.radio=esfera.radio-0.05f;
			}
		else esfera.radio=0.1f;
		
		if(puntos2==3)
		{
		printf("\n El jugador 2 ha ganado %d-%d al jugador 1\n",puntos2,puntos1);
		close(tuberia);
		exit(0);
		}
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		char cad[100];
		sprintf(cad," jugador 1 marco 1 punto,lleva %d \n",puntos1);
		write (tuberia,cad,strlen(cad)+1);

		if (esfera.radio>0.1f)
			{
				esfera.radio=esfera.radio-0.05f;
			}
		else esfera.radio=0.1f;

		if(puntos1==3)
		{
		printf("\n El jugador 1 ha ganado %d-%d al jugador 2\n",puntos1,puntos2);
		close(tuberia);
		exit(0);
		}

	}

//Read de la tuberia servidor-cliente y actualizacion de datos
	
	read(fifosc,buffersc,sizeof(buffersc));
	sscanf(buffersc,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);
	
	
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{

	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
	//Paso por la tuberia cliente-servidor las teclas pulsadas
	sprintf(buffercs, "%c", key);
	write (fifocs, buffercs, sizeof(buffercs)); 	

}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;


//Proyeccion en memoria bot
	//Abro fichero
	mem=open("/tmp/mem",O_CREAT|O_RDWR,0666);//creo el archivo y puedo leer y escribir.

	//Escribo
	write(mem,&datos,sizeof(datos));

	//Proyecto en memoria	
	m=(DatosMemCompartida*) mmap(NULL,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,mem,0);		

	//Cierro fichero
	close(mem);

//Tuberia comunicacion servidor-cliente

	mkfifo("/tmp/Tuberiasc",0777);
	fifosc=open("/tmp/Tuberiasc",O_RDONLY);

//Tuberia comunicacion cliente-servidor
	mkfifo("/tmp/Tuberiacs",0777);
	fifocs=open("/tmp/Tuberiacs",O_WRONLY);

//Esto solo es para el Servidor
//logger
//	tuberia=open("/tmp/TuberiaLogger",O_WRONLY);
}

